<?php

define('HOME', 'home/');

class Home{
    
   public function index()
    {
        $pageName = 'Home';
    
        include_once(TEMPLATES . HOME . 'index.php');
    }

}
