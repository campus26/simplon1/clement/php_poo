<?php
class Student{
 
    private $studentModel;
    function __construct() {
        include_once(__DIR__ . '/StudentModel.php');
        $this->studentModel=new StudentModel;
    }

    public function index($dbConnection)
{
    $this->studentModel->index($dbConnection); 
}

public function view($dbConnection)
{
    $studentId = getParam('studentId');
    $student = $this->findById($dbConnection, $studentId);
    $pageName = 'Students';
    $panelHeader = 'Student Information';

    include_once(TEMPLATES . 'student/view.php');
}

public function findById($dbConnection, $studentId)
{
   return $this->studentModel->findById($dbConnection,$studentId);   
}

public function add($dbConnection)
{
    $this->studentModel->add($dbConnection);   
}

public function edit($dbConnection)
{
    $studentId = getParam('studentId');
    $student = $this->findById($dbConnection, $studentId);

    echo json_encode($student);
}

public function update($dbConnection)
{
    $this->studentModel->update($dbConnection);   
}

public function delete($dbConnection)
{
    $this->studentModel->delete($dbConnection);   
}



}